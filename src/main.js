import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueRouter from "vue-router";
import App from './App.vue'
import routes from './router/index'
import store from './store'
import global from "./utils/global"

Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(VueRouter)

Vue.prototype.$EventBus = new Vue();

const router = new VueRouter({
    routes, // (缩写) 相当于 routes: route
})


router.afterEach((to, from) => {
    if(global.cancleHttp.length>0){        //强行中断时才向下执行
        global.cancleHttp.forEach(item=>{
            item();//给个标志，中断请求
        })  
    }
  })

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
