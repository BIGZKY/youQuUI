import page1 from "../pages/page1.vue";
import page2 from "../pages/page2.vue";
import page3 from "../pages/page3.vue";

const routes = [
    { path: '/', component: page2 },
    { path: '/page2', component: page2, name: 'page2' },
    { path: '/page3', component: page3, name: 'page3' },
    { path: '/page1', component: page1, name: 'page1' },
]



export default routes