import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import Vue from "vue"


// create an axios instance
const service = axios.create({
    baseURL: process.env.NODE_ENV === 'development' ? '' : '', // url = base url + request url
    timeout: 25000, // request timeout

})

// request interceptor
service.interceptors.request.use(
    config => {
        console.log('config', config);
        // do something before request is sent
        // if (store.getters.token) {
            // let each request carry token
            // ['X-Token'] is a custom headers key
            // please modify it according to the actual situation
            config.headers['X-Token'] = new Date().getTime()
        // }
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    response => {
        const res = response.data;
        // if the custom code is not 0, it is judged as an error.
        if (res.code !== 0) {
            // 3003 登录过期
            if (res.code === 3003) {
                // to re-login
                // MessageBox.confirm('登录已过期，是否重新登录？', '登录过期', {
                //     confirmButtonText: '重新登录',
                //     cancelButtonText: '取消',
                //     type: 'warning'
                // }).then(() => {
                // })
            } else {
                Message({
                    message: res.msg || 'Error',
                    type: 'error',
                    duration: 5 * 1000,
                    showClose: true
                })
                return Promise.reject(new Error(res.msg || 'Error'));
            }

        } else {
            return res
        }
    },
    error => {
        // console.log('err' + error) // for debug
        Message({
            message: error.message,
            type: 'error',
            duration: 5 * 1000,
            showClose: true
        })
        return Promise.reject(error)
    }
)

export default service